//
//  AppDelegate.m
//  ViettelMediaSDK
//
//  Created by kienlv on 1/8/21.
//

#import "AppDelegate.h"
#import <ViettelMediaSDK/ViettelMediaSDK.h>
#import "ViewController.h"
@interface AppDelegate () <UIApplicationDelegate>

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    ViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ViewController"]; //or the homeController
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
    self.window.rootViewController = navController;
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

    [ViettelMediaGameTracking start];
    [[NSNotificationCenter defaultCenter] addObserver:self
        selector:@selector(applicationDidBecomeActive:)
        name:UIApplicationDidBecomeActiveNotification
        object:nil];
    return YES;
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    [ViettelMediaGameTracking trackLaunchApp];
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(nonnull NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options {
    if ([[FBSDKApplicationDelegate sharedInstance] application:application
                                                       openURL:url
                                                       options:options]) {
        return YES;
    }
    if ([[GIDSignIn sharedInstance] handleURL:url]) {
        return YES;
    }
    return NO;
}


@end
