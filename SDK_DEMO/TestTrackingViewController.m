//
//  TestTrackingViewController.m
//  TestSDK
//
//  Created by Mac Mini on 02/11/2021.
//

#import "TestTrackingViewController.h"
#import <ViettelMediaSDK/ViettelMediaSDK.h>
@interface TestTrackingViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *data;
@end

@implementation TestTrackingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [ViettelMediaID shareInstance].userGameInfo = [[UserGameInfo alloc] initWithRoleID:@"1" roleName:@"thaigq" serverID:@"s1" roleLevel:@"20" roleOrderID:@"roolOrder"];
    
    self.data = @[
        @"vtm_resource_started",
        @"vtm_resource_finished",
        @"vtm_extract_started",
        @"vtm_extract_finished",
        @"vtm_extract_cdn_finished",
        @"vtm_enter_game_btn_clicked",
        @"vtm_character_created",
        @"af_tutorial_completion",
        @"vtm_character_join_game",
        @"af_level_achieved",
        @"vtm_vip_achieved",
        @"vtm_maintain_screen_opened",
    ];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:UITableViewCell.class forCellReuseIdentifier:@"cell"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _data.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.textLabel.text = self.data[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *type = self.data[indexPath.row];
    if ([type isEqualToString:@"vtm_resource_started"]) {
        [ViettelMediaGameTracking downloadResourceStarted];
    } else if ([type isEqualToString:@"vtm_resource_finished"]) {
        [ViettelMediaGameTracking downloadResourceFinished];
    }
    else if ([type isEqualToString:@"vtm_extract_started"]) {
        [ViettelMediaGameTracking extractStarteded];
    }
    else if ([type isEqualToString:@"vtm_extract_finished"]) {
        [ViettelMediaGameTracking extractFinished];
    }
    else if ([type isEqualToString:@"vtm_extract_cdn_finished"]) {
        [ViettelMediaGameTracking extractCDNFinished];
    }
    else if ([type isEqualToString:@"vtm_enter_game_btn_clicked"]) {
        [ViettelMediaGameTracking enterGameClickedWithGameServerId:@"serverid"];
    }
    else if ([type isEqualToString:@"vtm_character_created"]) {
        [ViettelMediaGameTracking trackCharacterCreated:@"characterId"
                                           gameServerId:@"serverId" characterName:@"name" characterLevel:@"level"];
    }
    else if ([type isEqualToString:@"af_tutorial_completion"]) {
        [ViettelMediaGameTracking trackingTutorialCompletion];
    }
    else if ([type isEqualToString:@"vtm_maintain_screen_opened"]) {
        [ViettelMediaGameTracking trackingMaintainScreenOpen];
    }
    else if ([type isEqualToString:@"vtm_character_join_game"]) {
        [ViettelMediaGameTracking trackingJoinGameWithScore:@"10"
                                               gameServerId:@"serverId"
                                                characterId:@"characterId"
                                              characterName:@"name"
                                             characterLevel:@"level"];
    }
    else if ([type isEqualToString:@"af_level_achieved"]) {
        [ViettelMediaGameTracking trackingFlyerLevelArchieved:@"score" level:@"level"];
    }
    else if ([type isEqualToString:@"vtm_vip_achieved"]) {
        [ViettelMediaGameTracking trackingUpgradeVip:@"levelVip"
                                         characterId:@"id"
                                        gameServerId:@"serverId"
                                       characterName:@"name"
                                      characterLevel:@"level"];
    }
  
}
- (IBAction)touchBack:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
    [ViettelMediaID shareInstance].userGameInfo = nil;
}

@end
