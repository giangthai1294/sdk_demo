//
//  AppDelegate.h
//  ViettelMediaSDK
//
//  Created by kienlv on 1/8/21.
//

#import <UIKit/UIKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
	
@property (strong, nonatomic) UIWindow *window;

@end

