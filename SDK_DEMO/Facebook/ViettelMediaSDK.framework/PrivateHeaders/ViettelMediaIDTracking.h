//
//  TrackingOject.h
//  ViettelMediaSDK
//
//  Created by kienlv on 1/19/21.
//

#import <Foundation/Foundation.h>
#import <AppsFlyerLib/AppsFlyerLib.h>
NS_ASSUME_NONNULL_BEGIN
@interface ViettelMediaIDTracking : NSObject

+ (instancetype)shared;
- (instancetype)initWithName:(NSString *)eventName;
- (ViettelMediaIDTracking*(^)(NSString *, NSString *))addParameterWithString;
- (ViettelMediaIDTracking*(^)(NSString *, BOOL))addParameterWithBool;
- (ViettelMediaIDTracking*(^)(NSString *, float))addParameterWithFloat;
- (ViettelMediaIDTracking*(^)(NSString *, int))addParameterWithInt;
+ (ViettelMediaIDTracking *)tagEvent:(NSString *)eventName;
+ (void)start;
- (void)setLoginTime;
- (void)trackLaunchApp;
- (void)trackFirstLaunchApp;
#pragma mark - appflyer tracking
- (void)trackingFlyerLoginSuccess: (NSString *) tab
                          typeApi: (NSString *) typeApi
                        loginType: (NSString *)loginType;
- (void)trackingFlyerIAP: (NSString *) orderNo;
- (void)trackingFlyerRegister: (NSString *)loginType;
- (void)trackCharacterCreatedCallApFailed;
- (void)trackCharacterCreatedCallApiSuccess;
#pragma mark - login tracking
- (void)trackingLoginSubmitted;
- (void)trackingRegisterFailed;
- (void)trackingLoginFailedType: (NSString *)loginType;
#pragma mark - apple tracking
- (void)loginAppleApiCall;
- (void)loginAppleApiCallFailed: (NSString *) errorCode
                   errorMessage: (NSString *) errorMessage;
- (void)touchLoginApple;
- (void)loginAppleFailed: (NSString *) errorCode
            errorMessage: (NSString *) errorMessage;
- (void)loginAppleCanceled;
#pragma mark - facebook tracking
- (void)loginFacebookApiCall;
- (void)loginFacebookApiCallFailed: (NSString *) errorCode
                      errorMessage: (NSString *) errorMessage;
- (void)touchLoginFacebook;
- (void)loginFacebookFailed: (NSString *) errorCode
               errorMessage: (NSString *) errorMessage;
- (void)loginFacebookCanceled;
#pragma mark - google tracking
- (void)loginGoogleApiCall;
- (void)loginGoogleApiCallFailed: (NSString *) errorCode
                    errorMessage: (NSString *) errorMessage;
- (void)touchLoginGoogle;
- (void)loginGoogleFailed: (NSString *) errorCode
             errorMessage: (NSString *) errorMessage;
- (void)loginGoogleCanceled;
#pragma mark - anonymous tracking
- (void)loginAnonymousApiCallFailed: (NSString *) errorCode
                       errorMessage: (NSString *) errorMessage;
- (void)touchLoginAnonymous;
#pragma mark - register tracking

- (void)trackingRegister;
- (void)trackingRegisterSubmitted;

#pragma mark - payment tracking
- (void) startVerifyPayment: (NSString *) orderNo itemID: (NSString *)itemID ;
- (void) verifyPaymentFailed: (NSString *) orderNo
                   errorType: (NSString *) type
                errorMessage: (NSString *) errorMessage
                   errorCode: (NSString *) errorCode;
- (void) trackingLoadPaymentFailed: (NSString *) errorMessage
                         errorCode: (NSString *) errorCode;

- (void) trackingLoadPaymentSuccess;
- (void) startLoadPayment;
- (void)clickPurchaseItem: (NSString *) itemId;

#pragma mark - game tracking
- (void)trackingJoinGameWithScore: (NSString *)score
                     gameServerId: (NSString *)gameServerId
                      characterId:(NSString *)characterId
                    characterName:(NSString *)characterName
                   characterLevel: (NSString *) characterlevel;
- (void)trackingUpgradeVip: (NSString *) vip
               characterId:(NSString *)characterId
              gameServerId: (NSString *)gameServerId
             characterName: (NSString *)characterName
            characterLevel: (NSString *) characterlevel;
- (void)downloadResourceFinished;
- (void)downloadResourceStarted;
- (void)extractCDNFinished;
- (void)extractFinished;
- (void)extractStarteded;
- (void)enterGameClickedWithGameServerId: (NSString *)gameServerId;
- (void)trackCharacterCreated: (NSString *)characterId
                 gameServerId: (NSString *)gameServerId
                characterName: (NSString *)characterName
               characterLevel: (NSString *) characterlevel;
- (void)trackingFlyerLevelArchieved: (NSString *) score
                              level: (NSString *) level;
- (void)trackingTutorialCompletion;
- (void)trackingMaintainScreenOpen;
@end

NS_ASSUME_NONNULL_END
