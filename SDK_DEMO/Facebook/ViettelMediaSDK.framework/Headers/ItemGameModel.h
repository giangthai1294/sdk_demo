//
//  ItemGameModel.h
//  SDKGame
//
//  Created by kienlv on 12/5/20.
//

#import "JSONModel.h"

@interface ItemGameModel : JSONModel

@property (nonatomic, copy) NSString *idItem;
@property (nonatomic, copy) NSString *productId;
@property (nonatomic, copy) NSString *gameItemId;
@property (nonatomic, copy) NSString *productName;
@property (nonatomic, assign) float price;
@property (nonatomic, assign) float priceApple;
@property (nonatomic, assign) float priceSaleOff;
@property (nonatomic, assign) float total;
@property (nonatomic, copy) NSString *smsShortCode;
@property (nonatomic, copy) NSString *serviceCode;
@property (nonatomic, copy) NSString *serviceName;
@property (nonatomic, copy) NSString *paymentMethodId;
@property (nonatomic, copy) NSString *smsContent;
@property (nonatomic, copy) NSString *syntaxCancel;

@end
