//
//  ViettelMediaGameTracking.h
//  ViettelMediaSDK
//
//  Created by Mac Mini on 03/11/2021.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface ViettelMediaGameTracking : NSObject
+ (void)start;
+ (void)trackLaunchApp;
+ (void)trackingFlyerLevelArchieved: (NSString *) score level: (NSString *) level;
+ (void)trackCharacterCreated: (NSString *)characterId
                 gameServerId: (NSString *)gameServerId
                characterName: (NSString *)characterName
               characterLevel: (NSString *) characterlevel;
+ (void)enterGameClickedWithGameServerId: (NSString *)gameServerId;
+ (void)extractFinished;
+ (void)extractStarteded;
+ (void)extractCDNFinished;
+ (void)downloadResourceStarted;
+ (void)downloadResourceFinished;
+ (void)trackingUpgradeVip: (NSString *) levelVip
               characterId:(NSString *)characterId
              gameServerId: (NSString *)gameServerId
             characterName: (NSString *)characterName
            characterLevel: (NSString *) characterlevel;
+ (void)trackingTutorialCompletion;
+ (void)trackingJoinGameWithScore: (NSString *)score
                     gameServerId: (NSString *)gameServerId
                      characterId:(NSString *)characterId
                    characterName:(NSString *)characterName
                   characterLevel: (NSString *) characterlevel;
+ (void)trackingMaintainScreenOpen;
@end

NS_ASSUME_NONNULL_END

