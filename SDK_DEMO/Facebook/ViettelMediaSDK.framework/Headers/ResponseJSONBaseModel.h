//
//  ResponseJSONBaseModel.h
//  ViettelMediaSDK
//
//  Created by kienlv on 1/26/21.
//

#import "JSONModel.h"

@interface ResponseJSONBaseModel : JSONModel

@property (nonatomic, assign) int code;
@property (nonatomic, copy) NSString *desc;

@end

