//
//  MarriedModel.h
//  ViettelMediaSDK
//
//  Created by kienlv on 1/11/21.
//

#import <Foundation/Foundation.h>


typedef enum {
    kNotMarrie = 0,
    kMarried = 1
}TypeMarrie;

NS_ASSUME_NONNULL_BEGIN

@interface MarriedModel : NSObject

@property (nonatomic, assign) TypeMarrie marrieStatus;
@property (nonatomic, strong) NSString *name;

+ (NSMutableArray <MarriedModel *> *)arrayRawData;
+ (NSMutableArray <NSString *> *)arrayData;
+ (NSString*)parseMarrieToString:(TypeMarrie)marrieStatus;

@end

NS_ASSUME_NONNULL_END
