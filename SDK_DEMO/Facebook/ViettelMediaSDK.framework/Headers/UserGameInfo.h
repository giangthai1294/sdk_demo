//
//  UserGameTracking.h
//  ViettelMediaSDK
//
//  Created by kienlv on 1/19/21.
//

#import <Foundation/Foundation.h>

@interface UserGameInfo : NSObject

@property (nonatomic, copy) NSString * _Nonnull roleID;
@property (nonatomic, copy) NSString * _Nonnull roleName;
@property (nonatomic, copy) NSString * _Nonnull serverID;
@property (nonatomic, copy) NSString * _Nonnull roleLevel;
@property (nonatomic, copy) NSString * _Nonnull roleOrderID;

- (instancetype _Nonnull )initWithRoleID:(NSString * _Nonnull)roleID
                                roleName:(NSString * _Nonnull)roleName
                                serverID:(NSString * _Nonnull)serverID
                               roleLevel:(NSString * _Nonnull)level
                             roleOrderID:(NSString * _Nonnull)orderID;

@end

