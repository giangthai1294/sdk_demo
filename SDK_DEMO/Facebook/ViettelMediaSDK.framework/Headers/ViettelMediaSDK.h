//
//  ViettelMediaSDK.h
//  ViettelMediaSDK
//
//  Created by kienlv on 1/27/21.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
//#import "ViettelMediaID.h"

//! Project version number for ViettelMediaSDK.
FOUNDATION_EXPORT double ViettelMediaSDKVersionNumber;

//! Project version string for ViettelMediaSDK.
FOUNDATION_EXPORT const unsigned char ViettelMediaSDKVersionString[];
#import "ViettelMediaID.h"
#import "ViettelMediaGameTracking.h"
// In this header, you should import all the public headers of your framework using statements like #import <ViettelMediaSDK/PublicHeader.h>


