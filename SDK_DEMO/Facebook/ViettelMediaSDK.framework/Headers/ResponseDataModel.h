//
//  AJBaseModel.h
//  Auto JSONModel
//
//  Created by kienlv on 11/27/20.
//  Copyright (c) 2015 [iF] Solution. All rights reserved.
//

#import "JSONModel.h"

@protocol ResponseDataModel
@end

@interface ResponseDataModel : JSONModel

@property (nonatomic, assign) int code;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *data;

@end
