//
//  AJUserModel.h
//  Auto JSONModel
//
//  Created by kienlv on 11/27/20.
//  Copyright (c) 2015 [iF] Solution. All rights reserved.
//

#import "JSONModel.h"
#import "GenderModel.h"
#import "MarriedModel.h"

@protocol AJUserModel

@end

@interface UserModel : JSONModel

@property (nonatomic, strong) NSString* username;
@property (nonatomic, strong) NSString* facebookId;
@property (nonatomic, strong) NSString* birthday;
@property (nonatomic, strong) NSString* deviceId;
@property (nonatomic, assign) TypeMarrie maritalStatus;
@property (nonatomic, strong) NSString* identifyPlace;
@property (nonatomic, strong) NSString* msisdn_protected;
@property (nonatomic, strong) NSString* verifyEmailStatus;
@property (nonatomic, strong) NSString* identifyDate;
@property (nonatomic, strong) NSString* identify;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* job;
@property (nonatomic, strong) NSString* accessToken;
@property (nonatomic, strong) NSString* tokenPayment;
@property (nonatomic, strong) NSString* id;
@property (nonatomic, strong) NSString* verifyMobileStatus;
@property (nonatomic, assign) TypeGender gender;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* verifyIdentifyStatus;
@property (nonatomic, strong) NSString* cityId;
@property (nonatomic, strong) NSString* avatar;
@property (nonatomic, strong) NSString* refreshToken;
@property (nonatomic, strong) NSString* email_protected;
@property (nonatomic, strong) NSString* googleId;
@property (nonatomic, strong) NSString* cityName;
@property (nonatomic, strong) NSString* address;
@property (nonatomic, strong) NSString* msisdn;
@property (nonatomic, strong) NSString* facebookName;
@property (nonatomic, strong) NSString* verifyToken;



@end
