//
//  ViettelMediaID.h
//  SDKGame
//
//  Created by kienlv on 11/27/20.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ResponseDataModel.h"
#import "UserModel.h"
#import "UserGameInfo.h"
#import "ItemGameModel.h"
NS_ASSUME_NONNULL_BEGIN
@interface ViettelMediaID : NSObject

@property (nonatomic, strong) UINavigationController *rootNavigationController;

@property (nonatomic, strong) UserGameInfo *userGameInfo;

+ (instancetype)shareInstance;

- (void)initCompletion:(void (^)(bool success)) completion;
- (void)loginSuccess:(void (^)(UserModel *user)) completion;
- (void)changeAvatar:(void (^)(NSString *avatar)) avatar;
- (void)loginFailed:(void (^)(ResponseDataModel *response)) completion;
- (void)logoutSuccess:(void (^)(void)) logout;
- (void)logoutFailed:(void (^)(void)) logout;
- (void)touchPolicy:(void (^)(void)) touchPolicy;

#pragma mark Action Show Controller

- (void)showLogin;
- (void)logout;
- (void)showUserInfo;
- (void)showListitem;
- (void)showLoginFromVC : (UIViewController *)vc;
- (void)onLogin;
- (void)onLogin : (UIViewController *)vc;

- (UserModel *)getUserInfo;

#pragma mark Config App
- (NSString *)clientKey;
- (NSString *)clientSecret;
- (NSString *)clientSecretPayment;
- (NSString *)mechantId;

#pragma mark Access Token TripDes
- (NSString *)getEncodeAccestokenTripDes;
- (NSString *)getSignEncryt;
- (NSNumber *)getTimeCreateSign;

#pragma mark Revew Token
- (void)reNewToken:(void (^) (void))renewSuccess failure:(void (^) (ResponseDataModel *)) renewFailure;

#pragma mark Save userinfor
- (void)saveUserInfo:(UserModel *)object;

#pragma mark IAP Helper
- (void)purchasesProduct:(ItemGameModel *)itemBundle
             gameOrderId: (NSString * __nullable)gameOrderId
              completion:(void (^)(void))success
                 failure:(void (^)(NSString *error))failure;

@end

NS_ASSUME_NONNULL_END
