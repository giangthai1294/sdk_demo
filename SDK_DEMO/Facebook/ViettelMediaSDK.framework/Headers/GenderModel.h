//
//  GenderModel.h
//  ViettelMediaSDK
//
//  Created by kienlv on 1/11/21.
//

#import <Foundation/Foundation.h>

typedef enum {
    kUnknown = 0,
    kMale = 1,
    kFemale = 2,
} TypeGender;

NS_ASSUME_NONNULL_BEGIN

@interface GenderModel : NSObject

@property (nonatomic, assign) TypeGender gender;
@property (nonatomic, strong) NSString *name;

+ (NSMutableArray <GenderModel *> *)arrayRawData;
+ (NSMutableArray <NSString *> *)arrayData;
+ (NSString*)parseGenderToString:(TypeGender)gender;

@end

NS_ASSUME_NONNULL_END
