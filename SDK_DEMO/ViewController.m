//
//  ViewController.m
//  SDKGame
//
//  Created by kienlv on 11/20/20.
//

#import "ViewController.h"
#import <ViettelMediaSDK/ViettelMediaSDK.h>
#import "TestTrackingViewController.h"
@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnLogout;
@property (weak, nonatomic) IBOutlet UIButton *btnPayment;
@property (weak, nonatomic) IBOutlet UIButton *userInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *listItemButton;
@property (assign, nonatomic) BOOL purchasing;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.btnLogin.hidden = NO;
    [self setupButton];
    [[ViettelMediaID shareInstance] initCompletion: ^(bool success){
        if(success){
            [[ViettelMediaID shareInstance] onLogin];

        }
    }];

    [[ViettelMediaID shareInstance] loginSuccess:^(UserModel *user) {
        if(user){
            self.btnLogin.hidden = YES;
            [self setupButton];
            NSLog(@"PTK_TOKEN_VERIFY ：%@", user.verifyToken);

        }else{
        }
     
    }];
    [[ViettelMediaID shareInstance] logoutSuccess:^ {
        self.btnLogin.hidden = NO;
        [self setupButton];
    }];
    [[ViettelMediaID shareInstance] touchPolicy:^{
        NSURL* url = [[NSURL alloc] initWithString: @"http://badlanders.vn/policy"];
        [[UIApplication sharedApplication] openURL: url];
    }];
    [[ViettelMediaID shareInstance] changeAvatar:^(NSString *avatar) {
        NSLog(@"PTK_CHANGE ：%@", avatar);

    }];
}

- (void)setupButton {
    self.userInfoButton.hidden = !self.btnLogin.isHidden;
    self.listItemButton.hidden = !self.btnLogin.isHidden;
//    self.btnPayment.hidden = !self.btnLogin.isHidden;
    self.btnLogout.hidden = !self.btnLogin.isHidden;
}

- (IBAction)touchLogin:(id)sender {
    [[ViettelMediaID shareInstance] onLogin];
}

- (IBAction)touchLogout:(id)sender {
    [[ViettelMediaID shareInstance] logout];
}
NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

-(NSString *) randomStringWithLength: (int) len {

    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];

    for (int i=0; i<len; i++) {
         [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }

    return randomString;
}
- (IBAction)touchPayment:(id)sender {
    if (self.purchasing) {
        return;
    }
    self.purchasing = true;

    ItemGameModel *itemModel = [[ItemGameModel alloc] init];
    itemModel.productId = @"1";
    [[ViettelMediaID shareInstance] purchasesProduct:itemModel gameOrderId:nil completion:^{
        self.purchasing = false;
    } failure:^(NSString *error) {
        self.purchasing = false;
    }];
}

- (IBAction)touchUserInfo:(id)sender {
    [[ViettelMediaID shareInstance] showUserInfo];
}

- (IBAction)touchListItem:(id)sender {
    [[ViettelMediaID shareInstance] showListitem];
    
}

- (IBAction)getData:(id)sender {
}
- (IBAction)tracking:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *myViewController = [storyboard instantiateViewControllerWithIdentifier:@"TestTrackingViewController"];
    [self presentViewController:myViewController animated:YES completion:nil];
    
}

@end
